package cz.cvut.fit.glazafil.annotations.buildin;

public class SomeOldSchoolLib {

    /**
     * @deprecated
     * As of release 1.6, replaced by coolMethod()
     */
    @Deprecated
    public void notSafeToUse() {

    }

    public void coolMethod() {

    }
}
