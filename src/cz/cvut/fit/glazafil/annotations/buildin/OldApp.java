package cz.cvut.fit.glazafil.annotations.buildin;

public class OldApp {

    public static void main(String[] args) {
        SomeOldSchoolLib lib = new SomeOldSchoolLib();
        lib.notSafeToUse();
    }

}
