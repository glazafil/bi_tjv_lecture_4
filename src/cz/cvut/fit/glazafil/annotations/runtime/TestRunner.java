package cz.cvut.fit.glazafil.annotations.runtime;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestRunner {

    public static void runTests(Object object) throws InvocationTargetException, IllegalAccessException {
        Class<?> givenClass = object.getClass();
        Method[] methods = givenClass.getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(Test.class)) {
                if (method.getAnnotation(Test.class).enabled()) {
                    System.out.println(method.getAnnotation(Test.class).comment());
                    method.invoke(object);
                }
            }
        }
    }


}
