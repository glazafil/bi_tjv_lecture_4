package cz.cvut.fit.glazafil.annotations.runtime;

import java.lang.reflect.InvocationTargetException;

public class Runtime {
    public static void main(String[] args) {
        TestCase testCase = new TestCase();
        try {
            TestRunner.runTests(testCase);
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
