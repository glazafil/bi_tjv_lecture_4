package cz.cvut.fit.glazafil.annotations.runtime;

public class TestCase {

    @Test
    public void testOne() {
        System.out.println("[Test] #1");
    }

    @Test(enabled = false)
    public void testTwo() {
        System.out.println("[Test] #2");
    }

    @Test(enabled = false, comment = "Third test")
    public void testThree() {
        System.out.println("[Test] #3");
    }

}
